package com.base.knowhow.forms;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArticleForm {

    private String description;

    private String title;
}

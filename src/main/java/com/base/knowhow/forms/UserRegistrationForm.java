package com.base.knowhow.forms;

import lombok.*;

import java.sql.Date;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationForm {

    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String role;
}
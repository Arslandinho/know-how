package com.base.knowhow.models;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "article",schema = "public")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "article_date")
    private String articleDate;

    @NotNull
    private String description;

    @NotNull
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

//    public Article getFirstNSymbols(Integer number) {
//        String content = this.description;
//        content = content.substring(0, number);
//
//        return new Article(id, articleDate, content, title);
//    }
}

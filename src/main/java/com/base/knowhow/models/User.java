package com.base.knowhow.models;



import com.base.knowhow.security.enums.Role;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user", schema = "public")
public class User {

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private Set<Article> articles;

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "firstname")
    @NotNull
    @Size(min = 1,max = 120)
    private String firstName;

    @Column(name = "lastname")
    @NotNull
    @Size(min = 1, max = 120)
    private String lastName;

    @Column(name = "email")
    @NotNull
    @Size(min = 1, max = 120)
    private String email;

    @Column(name = "password")
    @NotNull
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;
}
